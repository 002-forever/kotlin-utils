package boris.project.unsecure.routes

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class UnsecureRoutes (val routes: Array<String>)