package boris.project.unsecure.routes

import mu.KLogging
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
class UnsecureConfigurator : WebSecurityConfigurerAdapter() {
  companion object : KLogging()
  override fun configure(http: HttpSecurity) {
    var routesToUnsecure = getUnsecureAnnotations("boris.project").toTypedArray()
    http.authorizeRequests().antMatchers(*routesToUnsecure).permitAll();
    logger.info("Unsecured routes : ${routesToUnsecure.joinToString()}")
  }
}

fun getUnsecureAnnotations(basePackage: String): List<String> {

  val provider =
      ClassPathScanningCandidateComponentProvider(false)
  provider.addIncludeFilter(AnnotationTypeFilter(UnsecureRoutes::class.java))

  return provider.findCandidateComponents(basePackage)
      .map { Class.forName(it.beanClassName)
          .getAnnotation(UnsecureRoutes::class.java).routes.toList() }.flatten()
}