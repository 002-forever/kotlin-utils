package boris.project.utils.time

import java.text.SimpleDateFormat
import java.util.*

class UtilsTime {
  companion object {
    fun getUtcFormatted(pattern: String) : String {
      val formatter = SimpleDateFormat(pattern)
      formatter.timeZone = TimeZone.getTimeZone("UTC")
      return formatter.format(Date())
    }
  }
}