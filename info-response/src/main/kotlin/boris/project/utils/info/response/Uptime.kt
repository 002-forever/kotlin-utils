package boris.project.utils.info.response

import org.springframework.stereotype.Component
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs


@Component
class Uptime(val date: Date = Date(System.currentTimeMillis())) {
    fun getUptime(currentDate: Date): String {
        val diffInMillis = abs(currentDate.time - date.time)
        return return convertToTime(diffInMillis)
    }
    private fun convertToTime(dateDiff: Long): String {
        val date = Date(dateDiff)
        val formatter: DateFormat = SimpleDateFormat("HH:mm:ss.SSS")
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        return formatter.format(date)
    }
}