package boris.project.utils.info.response

import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Component
import java.util.*

@Component
@ComponentScan("boris.project.utils.info.response")
class InfoFactory(val uptime: Uptime) {
    fun createServiceInfo(serviceName: String): Info {
        return Info(serviceName, uptime = uptime.getUptime(Date(System.currentTimeMillis())));
    }
}