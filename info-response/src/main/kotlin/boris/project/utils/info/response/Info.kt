package boris.project.utils.info.response

import boris.project.utils.time.UtilsTime

class Info(
  val service: String,
  val status:String = "up",
  val uptime: String,
  val utc: String = UtilsTime.getUtcFormatted("yyyy-MM-dd HH:mm:ss")
)